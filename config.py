"""
config file for a grace app

see grace/config.py for available options to override here

note that config is loaded in this order:
- grace/config.py
- <appname>/config.py
"""
domains=["image","localhost","127.0.0.1"]
port = '9005'

additions_folder="/home/howie/image_additions"

show_time=False
