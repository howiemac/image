"""
images: override for grace/Page/Page.py

implements:
 - image addition
 - albums
 - tags

written by Ian Howie Mackenzie 2016 onwards
"""

from grace.Page import Page as basePage
from grace.lib import *
from grace.render import html

from copy import copy
import os, time, datetime, urllib.request, urllib.parse, urllib.error

class Page(basePage):
  ""
  contextkinds=['file'] #override - "image" removed from here

  # view override ####################

  @html
  def view_image(self,req):
    ""
    #req.wrapper=None

  @html
  def image_show(self,req):
    ""

  @html
  def view_album(self,req):
    ""
    #req.wrapper=None

  def view(self,req):
    ""
    if self.uid and self.uid==2: # additions album
      return self.additions(req)
    elif self.kind in ("root","album"): # all-images or other album
      return self.view_album(req)
#    elif self.kind=="image": # images
#      return self.view_image(req)
    return basePage.view(self,req) # root

  def latest(self, req):
    "overridden to include only child images, and to allow for additions"
    lim=page(req)
    where=f"{self.ratingclause(self.rating_filter())} and {self.branchclause()}"
    req.pages = self.list(kind='image',where=where,orderby="uid desc",limit=lim)
    req.title="latest"
    req.page='latest' # for paging
    return self.listing(req)

  def additions(self, req):
    "fetch new images (from page 2) and list them"
    # set rating filter to default - i.e. {0,1,2,3}
    self.set_rating_filter({0,1,2,3})
    # add new files from "image additions" folder
    self.add_images()
    # return thumbnail listing
    lim=page(req)
    where= f'rating in {sql_list(self.rating_filter())}'
    req.pages = self.list(kind='image',parent=2,where=where,orderby="uid desc",limit=lim)
    req.title="latest"
    req.page='additions' # for paging
    req.root=2 # for navigation
    if req.pages:
      return self.listing(req)
    # fallback if no pages found
    req.warning= "no additions found"
    return self.get(1).view(req)


  # edit override
  
  def edit(self,req):
    "page edit"
    return self.edit_tab(req)

  def save_text(self,req):
    "override of grace version - this is called by Page_edit_tab.evo"
    self.set_backlinks()
    self.update(req)
    self.flush_page(req)
    self.clear_form(req)
    return self.edit_return(req)

  def edit_return(self,req,view=""):
    """common return for edit options
     - redirect to optional view function
     - or else the default is "", ie view()
     - preserves req.tag, req.root if they exist
    """
#    extras={}
#    for k in ('tag','root'):
#      if (k in req) and req[k]:
#        extras[k]=req[k]
#    return req.redirect(self.url(view,**extras))
    return req.redirect(self.url(view,tag=req.tag,root=req.root))
  rating_return=edit_return # override grace.Page.ratings rating_return()


  # autostyle of image

  def autostyle(self,scale=100):
    "return a CSS style to autosize the image for use in evo template"
    return "width 20%"
    #height:100%; left:50%; margin-right:-50%; transform:translate(-50%,0)"

#WAS (for old style image stage (containing height, width etc):
#    d=self.get_stage_data()
#    w=safeint(d["full_width"])
#    h=safeint(d["full_height"])
#    if (w and h) and (w>h):
#      return "height:%d%%" % (h*scale//w,)
#    else:
#      return "height:%s%%" % scale

  # adding images ########################

  def add_images(self):
    '''
    fetch new images from image_additions folder, and allocate tags based on image folder name (if any)

    - assume that self.Config.additions_folder (default = "image_additions") will hold (in a tree) any new image files to be added
    - for each one:
      - add a new image page
      - fetch (move) the image file to the data folder
      - add tags based on dot-separated values in parent folder name
    '''
    # fetch some key data objects..
    # get a list of filepaths, extract image files, create image pages for them, moving the files to the ~/site/file folder
    root=True #use this to identify if we are in the root - ie the additions - folder
    for path,dirs,files in os.walk(self.Config.additions_folder):
      dirname=path.replace('\\','/').split("/")[-1] # (fix MS brain-dead slashes, and strip off path)
#      print("processing: ", dirname)  # path ,dirs, files
      for name in files:
        filepath=os.path.join(path,name)
        # get extension: ext
        exts=name.rsplit(".",1)
        if (len(exts)==2):
          ext=exts[1].lower()
        else:
          ext=""
        if ext in ('jpg','jpeg','gif','png','webp'):
          # create a new image page
          image=self.new()
          image.parent=2 #parent is always additions page (2)
          image.kind='image'
          image.seq=image.uid
#          image.update(req)
          image.set_lineage()
          image.text=os.path.join(dirname,name)
#          print "text=",image.text
          image.code="%s.%s" % (image.uid,ext)
          image.when=DATE(datetime.datetime.fromtimestamp(os.path.getmtime(filepath)))
#          print "modified:", image.when
          image.flush() #store the image page
##          image.renumber_siblings_by_kind()#keep them in order
          # move the image file
          os.renames(filepath,image.file_loc(image.code)) #BEWARE: this will remove the image_additions folder itself if it is now empty...
          # add tags (unless we are at the root of the file tree)
          if not root:
            for tag in dirname.split("."):
              image.add_tag(tag)
      root=False;
    return True

  # TAGS ################################
  # 
  # listing of tag results ##############

  def selecttag(self,req):
    " add to tag selection list for viewing"
    tags=[]
    if req.tag:
      tags=req.tag.split(",")
    if req.addtag:
      if req.addtag in tags: # negate it
        tags[tags.index(req.addtag)]="~"+req.addtag
      elif ("~"+req.addtag) in tags: # negate it
        tags[tags.index("~"+req.addtag)]=req.addtag
      else: # add the tag
        tags.append(req.addtag)
    if req.deltag and req.deltag in tags:
      tags.remove(req.deltag)
    req.tag=",".join(tags)
    return req.redirect(self.url("",tag=req.tag))

  def latest_pages(self,req,limit):
    "returns a list of image pages in descending uid order"
    where=f"{self.ratingclause(self.rating_filter())} and {self.branchclause()}"
    return self.list(kind='image',where=where,orderby="uid desc",limit=limit)

  def latest(self, req):
    "overridden to include only child images, and to allow for additions"
    req.pages = self.latest_pages(req,limit=page(req))
    req.title="latest"
    req.page='latest' # for paging
    return self.listing(req)

  def next_page(self,req,desc=True):
    """ return the next page for given req.tag and req.root

        Note: "next page" is previous uid, as uid is descending by default, so desc=True"
    """
    p=self.get(req.root) if req.root else self.get_pob()
    order=f'uid {desc and "desc" or ""}'
    if req.tag:
      pages=p.children_by_tag(
       tag=req.tag,
       order=order,
       limit="1",
       ratings=self.rating_filter(),
       above=(None if desc else self.uid),
       below=(self.uid if desc else None)
       )
      if pages:
        return pages[0]
      req._method='tagged'
      return p
    else: # no tag, so return next image by UID
      match=f"uid {desc and '<' or '>'} {self.uid}"
      where=f"{match} and {p.branchclause()} and {p.ratingclause(self.rating_filter())}"
#      print(">>>>>>> where=",where)
      pages=self.list(kind="image",where=where,limit=1,orderby=order)
      if pages:
        return pages[0]
#      if req.root==1: # return to additions page
#        req._method='additions'
#      else:
#        req._method=''
      req._method=''
      return p

  def next(self,req):
    " return the view (or req._method) of the next page (image) for given req.tag and req.root"
    p=self.next_page(req)
    return p.edit_return(req,req._method)

  def prev(self,req):
    " return the view (or req._method) of the previous page (image) for given req.tag and req.root"
    p=self.next_page(req,desc=False) 
    return p.edit_return(req,req._method)

  def newest(self,req):
    "return the newest image (ie max uid) which matches given req.tag and req.root"
    if not req.tag:
      pages=self.latest_pages(req,limit=1)
    elif req.tag=="UNTAGGED":
      pages=self.children_untagged(order="uid desc",limit=1)
    else:
      pages=self.children_by_tag(tag=req.tag,order="uid desc",limit=1,ratings=self.rating_filter())
    p = pages[0] if pages else self;
    return p.edit_return(req,req._method)

  @html
  def tag_stats(self,req):
    "statistics on the tags"

  # slideshow #############

  @html
  def show(self,req):
    "slideshow"

  def slideshow(self,req):
    "start a slideshow for given req.tag and req.root"
    if req.tag:
      first=self.children_by_tag(tag=req.tag,order="uid desc",limit=1,ratings=self.rating_filter())
    elif req.root:
      first=self.latest_pages(req,limit=1)
#    else: # show single image fullscreen
#      first=[self]
    if first:
      print("GOT FIRST ",first)
      return first[0].show(req)
    req.warning="no results found"
    return self.view(req)

# override of grace.Page.tabs.py version to pass req.root and req.tag

  def tag(self,req):
    "add tag of req.name for self"
    if req.name:
      name=urllib.parse.unquote_plus(req.name)
      self.add_tag(name)
    return req.redirect(self.url("",tag=req.tag,root=req.root))

  def untag(self,req):
    "remove tag of req.name from self"
    if req.name:
      name=urllib.parse.unquote_plus(req.name)
      self.delete_tag(name)
    return req.redirect(self.url("",tag=req.tag,root=req.root))

# override of grace version to exclude albums where rating is disabled
# NOTE grace version has been enhanced, which should still work here as before.... IHM Mar 2021
  def branchclause(self):
    "returns sql WHERE clause operator and parameters, depending on album (self.uid)"
    clause=("true") if (self.uid==1) else (f"parent={self.uid}")
    # get mysql-tuple of uids of enabled albums (the 0 is added in case all are disabled)
    albums = str([0]+self.album_uids()).replace('[','(').replace(']',')')
    clause+=f" and parent in {albums}"
    return clause

# override of grace  version for custom non-tab non-search display

#  def search(self,req):
#      "dummy - to disable grace search (until we have code to filter out disabled albums)"
#      return "no search available"

  def tagged(self,req,pagemax=50):
        " returns a listing of all images with given req.tag, in descending uid order"
        tag=req.tag
        if not tag:
            return self.latest(req)
        if tag=="UNTAGGED":
            return self.untagged(req,pagemax)
        limit=page(req,pagemax)
        req.pages=self.children_by_tag(tag=tag,order="uid desc",limit=limit,ratings=self.rating_filter())
        req.title=f'tagged "{tag}"'
        req.page='tagged' # for paging
#        print("req.pages:",req.pages) 
        return self.listing(req)

  def untagged(self,req,pagemax=50):
        " returns a listing of all pages with no tag "
        limit=page(req,pagemax)
        req.pages=self.children_untagged(order="uid desc",limit=limit)
        req.title='untagged'
        req.pages=self.children_untagged(order="uid desc",limit=limit)
        req.title='untagged'
        req.page='tagged' # for paging
        return self.listing(req)


# end of tags


## deletion of images and their pages ####################
#
#  def remove_image(self,req):
#    "for images only - deletes self and the related image file and tags"
#    if self.kind=="image":
#      # delete the file and the database instance
#      self.delete_image()
#      # delete the associated tags
#      self.delete_tags()
#      return req.redirect(self.get(1).url(""))
#    req.error="not an image - cannot remove this"
#    return req.redirect(self.url(""))


# ratings + disable/enable ################

  ratedkinds=("page","image","album") # override grace.Page.ratings version to include albums


# score ##################

  @html
  def offer(self,req):
    """ offer two images to select from: self, and req.rival
    """

  def select(self,req):
    """ select self and req.rival, to make an offer
        - starting self should be that of the previous offer (or any preferred starting place)
        if req.tag is specified then the results will be filtered using the first given tag 
    """
#    db=self.Config.database
#    sql="""select pages.uid from `%s`.pages
#             inner join `%s`.tags
#             on tags.page=pages.uid
#             where pages.kind="image"
#             and tags.name<>"girl"
#             and rating>=0
#             and pages.uid>%s
#             order by pages.uid
#             limit 2
#        """ % (db,db,self.uid)
##    print sql
#    pages=[p["uid"] for p in self.list(sql=sql,asObjects=False)]

    # get self
    tag = req.tag.split(".",1)[0] if req.tag else ""
    andclause = f' and uid in (select distinct page from `{self.Config.database}`.tags where name="{tag}")' if tag else ""
    basic_where = self.ratingclause(self.rating_filter())+andclause
    if self.kind == 'image': 
      where=basic_where+f" and uid>{self.uid}"
    else: # assume fist time
      where=basic_where
#    print("self",where) 
    selfs=self.list_int('uid',kind="image",where=where,orderby="uid",limit="1")
    # get rival
    if req.rival:
      where=basic_where+f" and uid<{req.rival}"
    else: # assume first time
      where=basic_where
#    print("rival",where) 
    rivals=self.list_int('uid',kind="image",where=where,orderby="uid desc",limit="1")
    # make and offer
    if selfs and rivals:
      return req.redirect(self.get(selfs[0]).url("offer",rival=rivals[0],tag=tag))
    # bomb out
    return req.redirect(self.get(1).url())

  def win(self,req):
    """ self has been chosen in preference to req.rival
        so adjust scores accordingly
    """
    rival=self.get(safeint(req.rival))
    # adjust scores - don't allow 0 to be re-used
    self.score+= (2 if self.score==-1 else 1)
    self.flush()
    rival.score-= (2 if rival.score==1 else 1)
    rival.flush()
    # return next offer
    return self.select(req)

  def lose(self,req):
    """ req.rival has been chosen in preference to self
        so adjust scores accordingly
    """
    rival=self.get(safeint(req.rival))
    # adjust scores - don't allow 0 to be re-used
    self.score-= (2 if self.score==1 else 1)
    self.flush()
    rival.score+= (2 if rival.score==-1 else 1)
    rival.flush()
    # return next offer
    return self.select(req)

# album

  def get_album(self):
    "return album object"
    if self.kind=='image':
      return self.get_pob()
    elif self.kind=='album':
      return self
    # else default to additions
    return self.get(2)
    
  def set_album(self,req):
    "set album-choice to req.album (or else set it to additions i.e. 2)"
    level=safeint(req.album) or 2
    self.move_to(req.album)
    return self.edit_return(req)

  def album_uids(self):
    "return a list of all enabled album uids"
    return self.list_int(kind="album",where="rating>=0",orderby="uid")

  @html
  def albums(self,req):
    """ list of albums and their details
    """
    self.data = self.list(kind="album", orderby="uid")

  def get_navbar_links(self):
    "override of grace standard to give navbar of albums"
    home=self.get(1) # all images
    links=[(home.name,home.url(),home.name)]
    for uid in self.album_uids():
      album=self.get(uid)
      links.append((album.name,album.url(),album.name+" album"))
    return links 

# file info

  def filesize(self):
    "returns filesize in bytes"
    return os.path.getsize(self.file_loc())


# utilities

  def remove_rejects(self,req):
    "delete all rejected images"
    c=0
    for i in self.list(rating=-5,kind="image"):
      i.delete()
      print("<deleted>",i.uid, i.text)
      c+=1
    return "%s rejected items deleted" % c


  def move_to(self,parent):
    "changes self.parent to parent"
    self.parent=parent
    self.set_lineage()
    self.flush()


  ignore='''  June 2022: the folllowing as  been replaced with the generic grace "pare_files
  @classmethod
  def pare_files(self,req):
    """prunes all obsolete files from the site/file folder
       (adapted from similar routine in mp)

       Optional request parameter: ?source=<your data folder>

       1) moves entire file hierarchy to an "ximage" destination folder
       2) then moves valid files back to source
       3) thus obsolete files remain in the "ximage" folder, for manual deletion

    WARNING: Assumes a proper filesystem (ie not fat or exfat!)
    """
    # data folder
    source=req.source or "/home/howie/files/image/"
    # rename the source folder, and create dest folder (as the original source)
    print('moving "image" folder to "ximage"')
    dest=copy(source)
    source=source.replace("/image/","/ximage/") # DODGY - will break if /image/ is duplicated in the path..
    os.rename(dest,source)
    # move the valid files to the original folder
    print("moving back the valid files...")
    c=0
    for i in self.list(isin={'kind':('file','image')},orderby="uid"):
      c+=1
      fn="%s/%s" % (i.file_folder(),i.code)
      print("keeping ",fn)
      os.renames(source+fn,dest+fn)
    print("done: ",c, " files retained")
    return "pare completed: %s files retained" % c
'''

# fixes

  def fix_parent(self,req):
    "change parent for self (or for child pages selected by req.tag) to req.to"
    t=req.tag
    p=safeint(req.to)
    if t:
      c=0
      for page in self.children_by_tag(req.tag,ratings=self.rating_filter()):
        c+=1
        page.move_to(p)
      return "%s pages moved to parent %s (%s)" % (c,p,self.get(p).name)
    elif p:
      self.move_to(p)
      return "page %s moved to parent %s (%s)" % (self.uid,p,self.get(p).name)
    return "?to=xxxx is required"

#  def fix(self,req):
#    ""
#    c=0
#    for tag in self.Tag.list(name="smoking"):
#      for t in self.Tag.list(page=tag.page,name="people"):
#        t.name="girl"
#        t.flush()
#      c+=1
#    return "%s tags changed" % c

  def fix_extra_tag(self,req):
    """add tag (req.tag) to every image tagged req.tagged
       - expects req.tagged (existing tagname) and req.tag (new tag to be added)
       - BEWARE: NOT TESTED
    """
    c=0
    for tag in self.Tag.list(tag=req.tag):
      image=self.get(tag.page)
      image.add_tag(req.tagged)
      c+=1
    return "%s tags added" % c

  def fix_custom(self,req):
    "custom tag fixes"
    images = self.list(parent=3, kind='image')
    for i in images:
      i.add_tag('divine')
    return f"{len(images)} tags fixed"
